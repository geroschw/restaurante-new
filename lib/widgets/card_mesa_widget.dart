import 'package:flutter/material.dart';
import 'package:restaurante/style/theme.dart' as Style;

class MesaCard extends StatefulWidget {
  final int nroMesa;
  final String estado;
  // final int monto;
  // final int cantidad;

  const MesaCard({
    Key? key,
    required this.nroMesa,
    required this.estado,
    // required this.monto,
    // required this.cantidad
  }) : super(key: key);

  @override
  _MesaCardState createState() => _MesaCardState();
}

class _MesaCardState extends State<MesaCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(
          top: 10.0,
          bottom: 10.0,
          left: 24.0,
          right: 24.0,
        ),
        child: Stack(
          children: <Widget>[
            _mesaCard(widget.nroMesa.toString(), widget.estado),
            _mesaThumbnail(_estadoMesa().getColor(widget.estado),
                _estadoMesa().getIcon(widget.estado)),
          ],
        ));
  }
}

_mesaThumbnail(Color? color, Icon icon) => Container(
      width: 90.0,
      height: 90.0,
      margin: const EdgeInsets.symmetric(vertical: 16.0),
      alignment: FractionalOffset.centerLeft,
      decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
          boxShadow: [Style.Shadows.sombraMain()]),
      child: Center(child: icon),
    );

_mesaCard(String nroMesa, String estado) => Container(
      height: 124.0,
      margin: const EdgeInsets.only(left: 46.0),
      decoration: BoxDecoration(
        color: Style.Colors.mainColor,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(17.0),
        boxShadow: [Style.Shadows.sombraMain()],
      ),
      child: _mesaCardContent(nroMesa, estado),
    );

_mesaCardContent(String nroMesa, String estado) => Container(
      margin: const EdgeInsets.fromLTRB(66.0, 10.0, 20.0, 16.0),
      // constraints: const BoxConstraints.expand(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          // Container(height: 4.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: Text(
                  'Mesa $nroMesa',
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 26.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
              // Container(width: 40.0),
              Text(
                estado,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          // Container(height: 32.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const Icon(Icons.people_alt_rounded, color: Colors.white),
              // Container(width: 8.0),
              Expanded(
                flex: 2,
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: const Text(
                  "5 personas",
                  style: TextStyle(
                      color: Color(0xffb6b2df),
                      fontSize: 14.0,
                      fontWeight: FontWeight.w400),
                ),
              )),
              const Icon(Icons.monetization_on_rounded, color: Colors.white),
              // Container(width: 8.0),
              Expanded(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: const Text(
                  '18.000',
                  style: TextStyle(
                      color: Color(0xffb6b2df),
                      fontSize: 14.0,
                      fontWeight: FontWeight.w400),
                ),
              ))
            ],
          ),
        ],
      ),
    );

class _estadoMesa {
  final _icon = <String, IconData>{
    'Esperando': Icons.watch_later_outlined,
    'Disponible': Icons.done_outline,
    'Servido': Icons.restaurant,
    'Cuenta': Icons.restaurant_menu,
    'Atencion': Icons.room_service
  };

  Icon getIcon(String estado) {
    return Icon(
      _icon[estado],
      color: Colors.black,
      size: 70.0,
    );
  }

  final _color = <String, Color>{
    'Esperando': const Color(0xFFFCCB82),
    'Disponible': const Color(0xFFB0FC82),
    'Servido': const Color(0xFFFC8282),
    'Cuenta': const Color(0xFFFC82E1),
    'Atencion': const Color(0xFF82FCFC)
  };

  Color? getColor(String estado) {
    return _color[estado];
  }
}
