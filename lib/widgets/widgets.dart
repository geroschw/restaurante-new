import 'package:flutter/material.dart';
import 'package:restaurante/style/theme.dart' as Style;

class AppBar1 {
  static appBar(titulo, leadingButton, actionButton, context) {
    return AppBar(
      title: Text(titulo,
          style: TextStyle(
              fontSize: Style.TextSizes.letraTitulos(context),
              color: Style.Colors.titleColor)),
      centerTitle: true,
      backgroundColor: Style.Colors.blanco,
      elevation: 0.0,
      leading: backButtonFunc(leadingButton, context),
      actions: [actionButtonFunc(actionButton, context)],
    );
  }
}

Widget backButtonFunc(bool leadingButton, BuildContext context) {
  return ((leadingButton)
      ? IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context))
      : Container());
}

Widget actionButtonFunc(String actionButton, BuildContext context) {
  /*return ((action)
      ? IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context))
      : Container());*/
  switch (actionButton) {
    case 'Filtrar':
      return FittedBox(
        child: IconButton(
            icon: const Icon(Icons.filter_alt),
            color: Style.Colors.acceptColor2,
            onPressed: () => Navigator.pop(context)),
      );
    default:
      return Container();
  }
}
