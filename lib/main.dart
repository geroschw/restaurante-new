// import 'package:flutter/material.dart';
// import 'package:restaurante/screens/detalle_mesa_screen.dart';
// import 'menuPrincipal.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MenuPrincipal(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key, required this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//     );
//   }
// }

// import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurante/menuPrincipal.dart';
import 'package:restaurante/screens/detalle_mesa_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    // ignore: invalid_use_of_visible_for_testing_member
    // SharedPreferences.setMockInitialValues({});
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: PaginaInicial(title: 'Restaurante App'),
    );
  }
}

class PaginaInicial extends StatefulWidget {
  const PaginaInicial({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<PaginaInicial> createState() => _PaginaInicialState();
}

List<Widget> children = [];

class _PaginaInicialState extends State<PaginaInicial> {
  int _currentIndex = 0;

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    var listaBotones = _establecerBotones();
    return Scaffold(
      body: children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTappedBar,
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.white,
        backgroundColor: Colors.grey,
        items: listaBotones,
      ),
    );
  }

  List<BottomNavigationBarItem> _establecerBotones() {
    List<BottomNavigationBarItem> listaBotones = [];

    children = [];
    listaBotones.add(
      const BottomNavigationBarItem(
        icon: Icon(Icons.dashboard),
        label: 'Pedidos',
      ),
    );
    children.add(MenuPrincipal());

    listaBotones.add(
      const BottomNavigationBarItem(
        icon: Icon(Icons.assessment),
        label: 'Reporteria',
      ),
    );
    children.add(DetalleMesa());

    listaBotones.add(
      const BottomNavigationBarItem(
        icon: Icon(Icons.supervised_user_circle),
        label: 'Mi cuenta',
      ),
    );
    children.add(MenuPrincipal());

    return listaBotones;
  }
}
