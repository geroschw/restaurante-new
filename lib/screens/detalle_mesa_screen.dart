import 'package:flutter/material.dart';
import 'package:restaurante/widgets/widgets.dart' as Widgets;
import 'package:expansion_tile_card/expansion_tile_card.dart';

class DetalleMesa extends StatefulWidget {
  const DetalleMesa({Key? key}) : super(key: key);

  @override
  _DetalleMesaState createState() => _DetalleMesaState();
}

class _DetalleMesaState extends State<DetalleMesa> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Widgets.AppBar1.appBar("Detalle", true, "", context),
        body: pedido());
  }
}

Widget pedido() {
  return ExpansionTileCard(
    baseColor: Colors.white,
    expandedColor: Colors.grey[50],
    expandedTextColor: Colors.black,
    leading: CircleAvatar(
      backgroundColor: Colors.green,
    ),
    title: Text("Producto"),
    subtitle: Text("Cantidad:"),
    children: <Widget>[
      // Divider(
      //   thickness: 1.0,
      //   height: 1.0,
      // ),
      // Align(
      //   alignment: Alignment.centerLeft,
      //   child: Padding(
      //     padding: const EdgeInsets.symmetric(
      //       horizontal: 16.0,
      //       vertical: 8.0,
      //     ),
      //     child: Text("Precio: "),
      //   ),
      // ),
      Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text('Precio: 200'), Text('Nota: ')],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              children: [Text('Hora de pedido: '), Text('Hora de entrega: ')],
            ),
          )
        ],
      )
    ],
  );
}
