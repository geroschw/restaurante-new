import 'package:flutter/material.dart';

class Colors {
  const Colors();
//para agregar un color hexadecimal por ej #357ebd agregarle al principio 0xFF y sacarle el numeral y luego el color -> 0xFF357edb

  //Azul Grandi
  static const Color mainColor = const Color(0xFF357ebd);

  //Gris claro
  static const Color secondColor = const Color(0xFFCCCCCC);

  //Negro
  static const Color titleColor = const Color(0xFF000000);

  //Rojo cancelar
  static const Color cancelColor = const Color(0xFFFF0000);

  static const Color blanco = const Color(0xFFFFFFFF);

  static const Color cancelColor2 = const Color(0xFFFF5252);

  static const Color acceptColor = const Color(0xFF8BC34A);

  static const Color acceptColor2 = const Color(0xFF4CAF50);

  static const Color botonesPrincipales = const Color(0xFF5698D9);
}

class Shapes {
//shape para botones grandes
  static RoundedRectangleBorder botonGrandeRoundedRectangleBorder() {
    return RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0));
  }
}

class TextSizes {
  static letraGrande(BuildContext context) {
    return MediaQuery.of(context).size.width * 0.05;
  }

  static letraChica(BuildContext context) {
    return MediaQuery.of(context).size.width * 0.04;
  }

  static letraTablas(BuildContext context) {
    return MediaQuery.of(context).size.width * 0.032;
  }

  static letraTitulos(BuildContext context) {
    return MediaQuery.of(context).size.width * 0.055;
  }
}

class ButtonThemes {
  static botonGrandeButtonTheme(
    context,
    IconData icon,
    String title,
  ) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height * 0.1),
      child: ElevatedButton.icon(
          style: ButtonStyle(
              shape: MaterialStateProperty.all(
                  Shapes.botonGrandeRoundedRectangleBorder())),
          onPressed: () {},
          icon: Icon(
            Icons.assignment_turned_in,
            color: Colors.secondColor,
            size: 40,
          ),
          label: Text(title,
              style: TextStyle(color: Colors.blanco, fontSize: 20))),
    );
  }

  static botonChicoButtonTheme(
    context,
    String title,
  ) {
    return ButtonTheme(
      buttonColor: Colors.mainColor,
      child: ElevatedButton(
          style: ButtonStyle(
              shape: MaterialStateProperty.all(
                  Shapes.botonGrandeRoundedRectangleBorder())),
          // style: ElevatedButton.styleFrom(
          //   shape: Shapes.botonGrandeRoundedRectangleBorder(),
          // ),
          onPressed: () {},
          child: Text(title,
              style: TextStyle(color: Colors.blanco, fontSize: 10))),
    );
  }
}

class Shadows {
  static sombraMain() {
    return BoxShadow(
        color: Colors.titleColor.withOpacity(0.3),
        blurRadius: 5.0,
        offset: const Offset(0.0, 4.0),
      );
  }
}