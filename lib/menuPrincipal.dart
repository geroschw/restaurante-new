import 'package:flutter/material.dart';
import 'package:restaurante/widgets/card_mesa_widget.dart';
import 'package:restaurante/widgets/widgets.dart' as Widgets;

class MenuPrincipal extends StatefulWidget {
  @override
  _MenuPrincipalState createState() => _MenuPrincipalState();
}

class _MenuPrincipalState extends State<MenuPrincipal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Widgets.AppBar1.appBar("Mesas", false, "Filtrar", context),
        body: ListView.builder(
            itemCount: 5,
            itemBuilder: (context, index) {
              return InkWell(
                child: _crearLista(index),
                onTap: (){}
              );
            }));
  }
}

Widget prueba() {
  return Text("hola mundo");
}

_crearLista(index) {
  List<MesaCard> mesas = [];

  mesas.add(const MesaCard(nroMesa: 1, estado: 'Servido'));
  mesas.add(const MesaCard(nroMesa: 2, estado: 'Disponible'));
  mesas.add(const MesaCard(nroMesa: 4, estado: 'Esperando'));
  mesas.add(const MesaCard(nroMesa: 5, estado: 'Cuenta'));
  mesas.add(const MesaCard(nroMesa: 7, estado: 'Atencion'));

  return mesas[index];
}
